package com.example.quiz5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.quiz5.HttpRequest.getRequest
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        recyclerView.layoutManager = GridLayoutManager(this, 2)

        adapter = RecyclerViewAdapter(items, object : OnClickCustom {
            override fun onClick(position: Int) {
                    seeInfo(position)
            }

        })

        recyclerView.adapter = adapter

        getRequest()

    }


    private fun seeInfo(position:Int) {
        val intent = Intent(this, BandInformation::class.java)
        intent.putExtra("bandInfo", items[position])
        startActivity(intent)
    }


    private fun parseJson(body:String) {

        val json = JSONArray(body)
        for (item in 0 until json.length()) {
            val data = json[item] as JSONObject
            val itemModel = ItemModel()
            itemModel.img_url = data.getString("img_url")
            itemModel.name = data.getString("name")
            itemModel.genre = data.getString("genre")
            itemModel.info = data.getString("info")
            items.add(itemModel)
            adapter.notifyDataSetChanged()
        }}


    private fun getRequest() = getRequest(
        HttpRequest.BANDS,
        object : CustomCallBack {
            override fun onFailure(response: String) {

            }

            override fun onResponse(response: String) {
                parseJson(response)
            }
//                val json = Gson().fromJson(response, Array<ItemModel>::class.java).toList()
//                for (element in json) {
//                    val band = ItemModel()
//                    element.img_url = band.img_url
//                    element.name = band.name
//                    element.genre = band.genre
//                    element.info = band.info
//
//                    items.add(band)
//
//                    adapter.notifyDataSetChanged() }
        }
//}
 )




}



