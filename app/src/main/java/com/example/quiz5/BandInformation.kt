package com.example.quiz5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_band_information.*

class BandInformation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_information)
        init()
    }

    private  fun init(){
        val bandInfo = intent.extras!!.getParcelable<ItemModel>("bandInfo")
        textView.text = bandInfo!!.name
        infoTextView.text = bandInfo.info

    }
}
