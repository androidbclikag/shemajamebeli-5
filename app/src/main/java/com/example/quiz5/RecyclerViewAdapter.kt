package com.example.quiz5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.bands_item_layout.view.*

class RecyclerViewAdapter (private val dataSet: ArrayList<ItemModel>, private val seeInfo:OnClickCustom):
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {



    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener {
        fun onBind() {
        itemView.setOnClickListener(this)

            val items = dataSet[adapterPosition]
            itemView.textView.text = items.name
            Glide.with(itemView).load(items.img_url).placeholder(R.mipmap.ic_launcher_round).into(itemView.imageView)


        }
        override fun onClick(v: View?) {
            seeInfo.onClick(adapterPosition)

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.bands_item_layout, parent, false)
    )

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()
    }



}